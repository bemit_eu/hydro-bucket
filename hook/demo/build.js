const {run, parallel} = require('@insulo/runner/pretty');
const colors = require('colors/safe');
const path = require('path');

const process = require('process');
const argv = require('minimist')(process.argv.slice(process.execArgv.length + 2));

const ROOT_DIR = __dirname;
const ASSET_DIR = ROOT_DIR + '/asset/';
const BUILD_DIR = ROOT_DIR + '/asset/out/';

const watch = argv.w || argv.watch;

const task = {
    sass: run(
        require('@formanta/build-task.sass').run, // task
        [
            ASSET_DIR + 'style/main.scss', // entry
            BUILD_DIR + 'style.min.css', // output
            watch,
            'compressed',// outputStyle
            ROOT_DIR,
            ['node_modules'],// includePaths
        ], // config for task
        colors.underline.blue('task--sass') // name for logging
    ),
    media: () => {
        // Asset Files like JPG, PNG, SVG, MP4 and Generic Copying for e.g. PDF
        return new Promise((resolve) => {
            const {MediaOptimizer} = require('@insulo/media-optimizer');
            // add default handler functions, must be activated through a config using them
            MediaOptimizer.constructor.handler_default = {
                png: () => require('@insulo/media-optimizer-handler-png'),
                jpg: () => require('@insulo/media-optimizer-handler-jpg'),
                svg: () => require('@insulo/media-optimizer-handler-svg'),
                handbrake: () => require('@insulo/media-optimizer-handler-handbrake'),
                dynamic: () => require('@insulo/media-optimizer-handler-dynamic'),
            };
            run(
                require('@formanta/build-task.media'),
                [
                    {
                        // to : from
                        [BUILD_DIR + 'media']: ASSET_DIR + 'media/'
                    }, // src
                    {
                        png: {
                            quality: 80,
                            files: ['**/*.png'],
                        },
                        jpg: {
                            quality: 80,
                            progressive: true,
                            files: ['**/*.{jpg,jpeg}']
                        },
                        svg: {
                            removeViewBox: false,
                            files: ['**/*.svg']
                        },
                        handbrake: {
                            // uses handbrake, must be installed as peer dep. on linux
                            optimize: true,
                            // framerate, 15 for most web
                            rate: 15,
                            // high no. = low quality
                            quality: 24.0,
                            // mp4, avi and more https://handbrake.fr
                            files: ['**/*.mp4']
                        },
                        dynamic: {
                            files: ['**/*.{pdf,gif}']
                        }
                    },
                    watch
                ],
                colors.underline.blue('task--media')
            )().then(result => {
                resolve(result);
            });
        });
    },
    webpack: () => {
        return new Promise((resolve) => {
            const taskWebpack = require('@formanta/build-task.webpack');
            taskWebpack.run([{
                    _use: require('@formanta/build-task.webpack-config-es6'),
                    mode: watch ? 'development' : 'production',
                    // Windows (known): `path.resolve` must be called on paths or webpack could stuck without a message/error
                    entry: {
                        main: path.resolve(ASSET_DIR + 'js/main.js')
                    },
                    output: {
                        filename: '[name].min.js',
                        path: path.resolve(BUILD_DIR)
                    },
                    devtool: 'source-map'
                }, /*{
                    _use: require('@formanta/build-task.webpack-config-jsx'),
                    mode: watch ? 'development' : 'production',
                    // Windows (known): `path.resolve` must be called on paths or webpack could stuck without a message/error
                    entry: {
                        main: path.resolve(ASSET_DIR + 'react/main.jsx'),
                        contact: path.resolve(ASSET_DIR + 'react/contact.jsx')
                    },
                    output: {
                        filename: '[name].js',
                        path: path.resolve(BUILD_DIR + 'react')
                    },
                    optimization: {
                        splitChunks: {
                            chunks: 'all',
                            name(module, chunks, cacheGroupKey) {
                                // todo check naming with complexer entrypoints
                                return cacheGroupKey;
                            }
                        },
                    },
                    devtool: 'source-map'
                }*/],
                watch ? {
                    watch: {
                        aggregateTimeout: 300,
                        ignored: ['node_modules'],
                        poll: true
                    }
                } : {}
            )().then(() => {
                resolve();
            });
        });
    },
};

return run(
    parallel([
        task.sass,
        task.media,
        task.webpack,
    ]),
    [],
    'build'
)().then(() => {
    if(!watch) {
        process.exit();
    }
});