<?php

namespace DemoHook;

use Flood\Captn;
use Flood\Component\Route\Hook\HookFile;
use Flood\Component\Route\Hook\HookFileI;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\User\AccessManager\Role;

class HookDemo extends HookFile implements HookFileI {
    public function __construct() {
        Captn\EventDispatcher::on(
            'hydro.exec.bindComponent',
            [$this, 'addContent']
        );

        Captn\EventDispatcher::on(
            'hydro.exec.bindComponent',
            [$this, 'setupAccessControl']
        );
    }

    public function addContent() {

        // Register All Used Schemas (e.g. for Content\PageType, Shop\ProductType)
        FeatureContainer::_contentSchema()::register('meta', __DIR__ . '/data/_schema/meta.json');
        FeatureContainer::_contentSchema()::register('wine-item', __DIR__ . '/data/_schema/wine-item.json', $this->getId());

        // Content specific
        FeatureContainer::_contentPageType()::register('default', __DIR__ . '/data/_page-type/default.json');
        FeatureContainer::_contentPageType()::register('default2', __DIR__ . '/data/_page-type/default2.json', $this->getId());

        FeatureContainer::_contentBlock()::register('intro', __DIR__ . '/block/intro/block.json');
        FeatureContainer::_contentBlock()::register('mainText', __DIR__ . '/block/mainText/block.json');
        FeatureContainer::_contentBlock()::register('title', __DIR__ . '/block/title/block.json');
        FeatureContainer::_contentBlock()::register('gallery', __DIR__ . '/block/gallery/block.json', $this->getId());
        FeatureContainer::_contentBlock()::register('opening', __DIR__ . '/block/opening/block.json');

        // Shop specific
        FeatureContainer::_shopProductType()::register('wine-item', __DIR__ . '/data/_product-type/wine.json');
        FeatureContainer::_shopProductType()::register('event-item', __DIR__ . '/data/_product-type/wine.json', $this->getId());

        // Register existing mailings
        FeatureContainer::_mailings()::register('contact', __DIR__ . '/data/_mailing/contact.json');
    }

    public function setupAccessControl() {
        // bind the ability `hook.api.view` to this hook, enables everything for default roles, like defined in `HydroApi\HookHydroApi.setupAccessControl`
        FeatureContainer::_accessManager()
                        ->hookBindAbility($this->getId(), 'hook.api.view');
    }
}