<?php

namespace DemoHook\App\Controller;

use DemoHook\App\Service\ControllerResponse;
use Hydro\Container;
use HydroFeature\Container as FeatureContainer;

class Home extends ControllerResponse {

    public function __construct($payload = []) {
        parent::__construct($payload);
    }

    public function call() {
    }

    public function respond($template = '') {
        if(!empty($template)) {
            return parent::respond($template);
        } else {
            return parent::respond('Home.twig');
        }
    }
}