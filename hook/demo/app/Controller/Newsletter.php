<?php

namespace DemoHook\App\Controller;

use DemoHook\App\Service\ControllerResponse;
use Hydro\Container;
use Hydro\Input\Receive;
use HydroFeature\Container as FeatureContainer;

class Newsletter extends ControllerResponse {

    public function __construct($payload = []) {
        parent::__construct($payload);
    }

    public function call() {
        $subscribing = false;
        $subscribed = false;
        $unsubscribed = false;

        if('POST' === Container::_route()->request_context->getMethod()) {
            // subscribing

            $receive = new Receive();
            $email = $receive->get('email', FILTER_VALIDATE_EMAIL);

            if($email) {
                $newsletter = FeatureContainer::_newsletterRegistrationHandler();
                $newsletter->setLocale($this->active_response->locale);

                $newsletter->email = trim($email);

                $mailbox = FeatureContainer::_mailboxes()::get('default');

                if($newsletter->subscribe(
                    function($user, $confirm_link) {
                        return $this->render('mail/newsletter_optin.twig', ['user' => $user, 'confirm_link' => $confirm_link]);
                    },
                    $mailbox)) {
                    $subscribing = true;
                }
            } else {
                error_log('Newsletter reg email not valid.');
            }
        } else {
            $action = false;
            if(isset(Container::_route()->match['action'])) {
                $action = Container::_route()->match['action'];
            }
            $token = false;
            if(isset(Container::_route()->match['token'])) {
                $token = Container::_route()->match['token'];
            }
            if($action && $token) {
                $newsletter = FeatureContainer::_newsletterRegistrationHandler();
                $newsletter->setLocale($this->active_response->locale);

                if('confirm' === $action) {
                    if($newsletter->confirm($token)) {
                        $subscribed = true;
                    }
                } else if('unsubscribe' === $action) {
                    if($newsletter->unsubscribe($token)) {
                        $unsubscribed = true;
                    }
                }
            }
        }

        $this->assignTemplateData('nl_subscribing', $subscribing);
        $this->assignTemplateData('nl_subscribed', $subscribed);
        $this->assignTemplateData('nl_unsubscribed', $unsubscribed);
    }

    public function respond($template = '') {
        if(!empty($template)) {
            return parent::respond($template);
        } else {
            return parent::respond('Home.twig');
        }
    }
}