<?php

namespace DemoHook\App\Controller;

use DemoHook\App\Service\ControllerResponse;
use HydroFeature\Container as FeatureContainer;
use HydroFeature\Mailer\Mailer;

class Contact extends ControllerResponse {
    public function __construct($payload = []) {
        parent::__construct($payload);

    }

    public function call() {
        parent::call();
        if(filter_has_var(INPUT_POST, 'action')) {
            $data = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            if(
                isset($data['user']) && !empty($data['user']) &&
                isset($data['user']['name']) && !empty($data['user']['name']) &&
                isset($data['user']['email']) && !empty($data['user']['email']) &&
                isset($data['contact']['message']) && !empty($data['contact']['message'])
            ) {
                $message = FeatureContainer::_message()->create();
                $message->type = 'contact';
                $message->hook = $this->active_response->hook;

                $message->sender = FeatureContainer::_message()->user();
                $message->sender->name = $data['user']['name'];
                $message->sender->email = $data['user']['email'];

                $mailbox = FeatureContainer::_mailboxes()::get('default');
                if(!$mailbox) {
                    error_log('Mailbox `default` doesn\'t exist');
                    return;
                }
                $message->recipient = FeatureContainer::_message()->user();
                $message->recipient->name = 'Sitemanager';
                $message->recipient->email = $mailbox->getFromEmail();

                $message->subject = '[Contact] Homepagerequest';
                $message->text = $data['contact']['message'];
                $message->setTime();

                $saved = $message->create();
                if($saved) {
                    $mailer = new Mailer($mailbox);
                    $sender = $mailer->message($message->subject);

                    /*$sender = $message->send($mailbox, FeatureContainer::_mailings()::get('contact', $this->active_response->hook), [
                        'message' => $message,
                    ]);*/

                    $msg = $this->render('mail/contact.twig', [
                        'message' => $message,
                    ]);

                    $sender->setFrom($mailbox->getFromEmail(), $mailbox->getFromName())
                           ->setTo('recipient@example.org')
                           ->setBody($msg, 'text/html');

                    $success = $mailer->send($sender);

                    if($success) {
                        $this->assignTemplateData('submit_result', true);
                    } else {
                        $this->assignTemplateData('submit_result', false);
                    }
                } else {
                    $this->assignTemplateData('submit_result', false);
                }

                /**
                 * @var \Swift_Plugins_Loggers_ArrayLogger $log
                 */
                //$log = $success['log'];
                //var_dump($log->dump());
                //$this->assignTemplateData('submit_result_tmp', $success['success']);

            } else {
                $this->assignTemplateData('submit_result', false);
            }
        }
    }

    public function respond($template = '') {
        if(!empty($template)) {

            return parent::respond($template);
        } else {

            return parent::respond('Contact.twig');
        }
    }
}