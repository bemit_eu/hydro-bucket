<?php

namespace DemoHook\App\Controller;

use DemoHook\App\Service\ControllerResponse;
use Hydro\Container;
use Hydro\Input\Receive;
use HydroFeature\Container as FeatureContainer;

class Order extends ControllerResponse {

    public function __construct($payload = []) {
        parent::__construct($payload);
    }

    public function call() {
        $shop = FeatureContainer::_shop();
        $products = $shop->getProductDataForLocale($this->active_response->locale, true);
        $this->assignTemplateData('shop_products', $products);
        if('POST' === Container::_route()->request_context->getMethod()) {
            $success = false;

            $receive = new Receive();
            $products_selected = $receive->get('products', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
            $user = $receive->get('user', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

            if($user && $products_selected) {
                $order_builder = FeatureContainer::_shopOrderBuilder();
                $order_builder->setHook($this->active_response->hook);
                $order_builder->setLocale($this->active_response->locale);

                $order_builder->customer->account = false;
                if(isset($user['name'])) {
                    $order_builder->customer->name = trim($user['name']);
                }

                if(isset($user['email'])) {
                    $order_builder->customer->email = trim($user['email']);
                }

                foreach($products_selected as $product_data_id => $qty) {
                    $order_builder->addProduct($product_data_id, $qty);
                }

                $mailbox = FeatureContainer::_mailboxes()::get('default');
                try {
                    $order = $order_builder->save();
                    if(!$order) {
                        throw new \Exception('Order could not be saved');
                    }
                    $mail_msg = $this->render('mail/order.twig', ['order' => $order]);

                    if($order_builder->mail($mailbox, 'Order ' . $order_builder->order_id, $mail_msg)) {
                        $success = true;
                    }
                } catch(\Exception $e) {
                    error_log('ControllerOrder: order saving failed');
                }

            } else {
                error_log('Order without products');
            }

            $this->assignTemplateData('order_success', $success);
        }
    }

    public function respond($template = '') {
        if(!empty($template)) {
            return parent::respond($template);
        } else {
            return parent::respond('Order.twig');
        }
    }
}