<?php

namespace DemoHook\App\Controller;

use DemoHook\App\Service\ControllerResponse;
use Hydro\Container;
use HydroFeature\Container as FeatureContainer;

class ProductList extends ControllerResponse {

    public function __construct($payload = []) {
        parent::__construct($payload);
    }

    public function call() {
        $shop = FeatureContainer::_shop();
        $products = $shop->getProductDataForLocale($this->active_response->locale);
        $this->assignTemplateData('shop_products', $products);
    }

    public function respond($template = '') {
        if(!empty($template)) {
            return parent::respond($template);
        } else {
            return parent::respond('ProductList.twig');
        }
    }
}