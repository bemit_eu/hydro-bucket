<?php

namespace DemoHook\App\Service;

use Hydro\Container;

class ControllerResponse extends \Hydro\Controller\ResponseController {


    public function __construct($payload = []) {

        $this->template_namespace = 'demo-hook';
        $this->route_cdn = 'demo-hook-asset';
        $this->route_home = 'home';

        parent::__construct($payload);

        /*
        Container::_i18n()->addDictionaryDir(
            Container::_config()->serverPath(true) . 'hook/demo/data/i18n',
            'demo-hook'
        );
        */
    }

    public function call() {
    }
}