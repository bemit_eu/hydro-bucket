$(function () {
    var menu_expand = new MenuExpand();
    menu_expand.bind();

    var menu_section = new MenuSection();
    menu_section.bind();

    var menu = new Menu();
    menu.bind();
});