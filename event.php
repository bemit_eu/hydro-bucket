<?php

use Hydro\Container;
use HydroFeature\Container as FeatureContainer;
use HydroApi\BaseController\OptionsController;
use Flood\Captn;

/*
 * These Events are loaded AFTER all composer/components events have been registered, and before they are executed, but are executed after them
 */
Captn\Acknowledge::signal(
    'demo-hook',
    'root.demo-hook',
    Captn::TYPE__HOOK,
    [
        'hook_file' => [
            'register' => 'DemoHook\HookDemo',
            'json' => __DIR__ . '/hook/demo/HookFile.json',
        ],
    ]
);

Captn\Acknowledge::signal(
    'hydro-install',
    'root.hydro-install',
    Captn::TYPE__HOOK,
    [
        'hook_file' => [
            'register' => 'HydroInstall\HookHydroInstall',
            'json' => __DIR__ . '/vendor/flood/hydro-install/HookFile.json',
        ],
    ]
);

Captn\Acknowledge::signal(
    'hydro-api',
    'root.hydro-api',
    Captn::TYPE__HOOK,
    [
        'hook_file' => [
            'register' => 'HydroApi\HookHydroApi',
            'json' => __DIR__ . '/vendor/flood/hydro-api/HookFile.json',
        ],
    ]
);

Captn\EventDispatcher::on(
    'hydro.exec.bindComponent',
    static function() {
        \HydroFeature\User\Token::$max_age = 1200;
        // change on each installation
        //\HydroFeature\User\Token::$secret = \HydroFeature\User\Token::$secret;
        \HydroFeature\User\Token::secretFromEnv();

        Container::_cdn()::$config->addData(\Flood\Component\Json\Json::decodeFile(__DIR__ . '/vendor/flood/component-cdn/config.example.json', true));
        Container::_cdn()::$config->serverPath(__DIR__ . '/');

        Container::_config()::setEnv(__DIR__);

        \Flood\Component\Route\Route::$DEV_SERVER = explode(':', $_SERVER['HTTP_HOST'])[0];
        Container::_route()->devAddServer(__DIR__ . '/dev_server.conf');

        // todo: add to hook-file/implement multiple hooks with upload-restrictions per-dir
        FeatureContainer::_fileManager()::$upload_root = __DIR__ . '/upload';

        OptionsController::$origin_allowed[] = 'http://localhost:3000';
        OptionsController::$origin_allowed[] = 'http://localhost:3003';
        OptionsController::$origin_allowed[] = 'http://localhost:9220';
        OptionsController::$origin_allowed[] = 'https://admin.demo.canal.bemit.codes';
        OptionsController::$origin_allowed[] = 'https://nightly.admin.demo.canal.bemit.codes';
        OptionsController::$origin_allowed[] = 'https://nightly.admin.demo.hydro.bemit.codes';

        // Register used mailbox config
        FeatureContainer::_mailboxes()::register('default', __DIR__ . '/config/mailbox/example.json');

        // Register roles that should be able to choose from Api/Admin
        FeatureContainer::_userRoles()
                        ::register(\HydroFeature\User\AccessManager::ROLE_MANAGER)
                        ::register(\HydroFeature\User\AccessManager::ROLE_CUSTOMER)
                        ::register('manager-support')
                        ::register('support');
    }
);