<?php

/**
 * Only init the needed debugs in this file
 */

\Hydro\Hydro::$env = 'development';
//\Hydro\Hydro::$env = 'production';
\Hydro\Hydro::$debug = true;
\Hydro\Cache\Cache::$debug = false;

\Flood\Component\Cdn\CDN::$debug = false;
\Flood\Component\Route\Route::$debug = false;
\Flood\Component\Json\Json::$debug = false;