<?php

ini_set('display_errors', 0);

if(php_sapi_name() == 'cli-server') {
    $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
    $port = $_SERVER['SERVER_PORT'];
    $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
    error_log(
        'Hydro: Running on CLI Server for ' .
        'http://' . (
        isset($_SERVER['HTTP_X_FORWARDED_HOST']) ?
            $_SERVER['HTTP_X_FORWARDED_HOST'] :
            (isset($_SERVER['HTTP_HOST']) ?
                $_SERVER['HTTP_HOST'] :
                $_SERVER['SERVER_NAME'] . $port)
        ) . $_SERVER['REQUEST_URI']
    );
}

require __DIR__ . '/flow.php';