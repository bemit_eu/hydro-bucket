<?php

error_reporting(E_ALL);
ini_set('memory_limit', '2048M');
ini_set('max_execution_time', '360');

$option = [
    'performance' => [
        'time' => microtime(true), // Set time value for performance monitor
        'memory' => memory_get_usage(), // Set memory value for performance monitor
        'log' => (php_sapi_name() == 'cli-server'),
    ],
];

try {
    // setting const values, like for autoloader
    require dirname(__DIR__) . '/const.php';

    // load system file classes and composer packages
    // will execute `register.php` in most flood packages
    require dirname(__DIR__) . '/vendor/flood/hydro/autoload.php';

    // setting the needed debugging tools
    require dirname(__DIR__) . '/debug.php';

    // get registered core events
    require dirname(__DIR__) . '/event.php';

} catch(\Exception $e) {
    echo hydroFatalError('Hydro Boot Files Error', $e);;
    exit(1);
}

$flow = require_once dirname(__DIR__) . '/vendor/flood/hydro/flow.php';
$flow($option);