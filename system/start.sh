#!/bin/bash

while IFS='= ' read port domain
do
    if [[ ${domain} ]]
    then
        echo -e  "Starting Port:" ${port} "for domain" ${domain}
        php -S 0.0.0.0:${port} -c php.ini ./server.php display_errors=0 &
    fi
done < ../dev_server.conf

wait