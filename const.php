<?php

/*
 * Autoload const values which are used from `Flood\Components` and general ui-components, modules and more.
 * To turn off autoloading set some constant with the value `false`
 * autoloading in root components is enabled by default if installed through composer, utilizes composer.json `autoload: files: register.php`
 *
 * Syntax for constants is:
 * - root packages: the full namespace of the entry module/mainnamespace used, `\` to `_`, uppercase, `__` as action separator, action like `AUTOLOAD`
 */

define('CAPTN_IS_STEERING', true);

/**
 * @deprecated will be removed, once refactored out of core
 */
define('HYDRO_WD', __DIR__ . '/');

define('FLOOD_COMPONENT_PERFORMANCEMONITOR_MONITOR__AUTOLOAD', true);

define('FLAG__HYDRO_FLOW__PERF_LOG', true);