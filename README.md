# README #

Hydro Bucket

`composer create-project flood/hydro-bucket`

# Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

    2017-2018 - bemit UG (haftungsbeschränkt) - project@bemit.codes